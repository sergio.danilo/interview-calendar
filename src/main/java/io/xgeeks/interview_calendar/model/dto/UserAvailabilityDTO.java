package io.xgeeks.interview_calendar.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAvailabilityDTO {

    private Long userId;
    private List<Slot> slots;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Slot {
        private String day;
        private List<Time> times;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Time {
        private String fromTime;
        private String toTime;
    }

}
