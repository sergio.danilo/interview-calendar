package io.xgeeks.interview_calendar.model.dto;

import lombok.Builder;
import lombok.Data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class AvailableSlotsDTO {

    private LocalDate date;
    private DayOfWeek dayOfWeek;
    private LocalTime from;
    private LocalTime to;

}
