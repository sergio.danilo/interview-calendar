package io.xgeeks.interview_calendar.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message + " not found!");
    }

}
