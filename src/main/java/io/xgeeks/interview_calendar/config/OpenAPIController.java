package io.xgeeks.interview_calendar.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OpenAPIController {

    @GetMapping("/")
    String index() {
        return "redirect:/swagger-ui.html";
    }

}

