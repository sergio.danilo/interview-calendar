package io.xgeeks.interview_calendar.service;

import io.xgeeks.interview_calendar.exception.UserNotFoundException;
import io.xgeeks.interview_calendar.model.dto.AvailableSlotsDTO;
import io.xgeeks.interview_calendar.model.dto.UserAvailabilityDTO;
import io.xgeeks.interview_calendar.model.entity.TimeSlot;
import io.xgeeks.interview_calendar.model.entity.User;
import io.xgeeks.interview_calendar.model.entity.UserAvailability;
import io.xgeeks.interview_calendar.repository.UserAvailabilityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserAvailabilityService extends BaseService {

    private final UserAvailabilityRepository userAvailabilityRepository;
    private final UserService userService;

    public UserAvailabilityService(UserAvailabilityRepository userAvailabilityRepository, UserService userService) {
        this.userAvailabilityRepository = userAvailabilityRepository;
        this.userService = userService;
    }

    public void save(List<UserAvailabilityDTO> userAvailabilityDTOs) {
        userAvailabilityDTOs.forEach(userAvailabilityDTO -> {
            try {
                User user = userService.findById(userAvailabilityDTO.getUserId());

                userAvailabilityDTO.getSlots().forEach(slot -> {
                    List<TimeSlot> timeSlots = slot.getTimes().stream().map(this::buildTimeSlot)
                            .collect(Collectors.toList());

                    UserAvailability userAvailability = UserAvailability.builder()
                            .user(user)
                            .day(LocalDate.parse(slot.getDay()))
                            .timeSlots(timeSlots)
                            .build();

                    userAvailabilityRepository.save(userAvailability);
                });

            } catch (UserNotFoundException e) {
                log.warn(e.getMessage());
            }
        });
    }

    public List<UserAvailability> findByUser(Long userId) {
        return userAvailabilityRepository.findAllByUser_Id(userId);
    }

    public List<AvailableSlotsDTO> getAvailableSlots(List<Long> ids) {
        List<List<AvailableSlotsDTO>> allSlots = new ArrayList<>();

        ids.forEach(id -> {
            try {
                User user = userService.findById(id);
                List<UserAvailability> userAvailabilities = findByUser(user.getId());
                List<AvailableSlotsDTO> userAvailableSlot = new ArrayList<>();

                userAvailabilities.forEach(userAvailability -> {
                    userAvailability.getTimeSlots().forEach(timeSlot -> {
                        userAvailableSlot.add(
                                AvailableSlotsDTO.builder()
                                        .date(userAvailability.getDay())
                                        .dayOfWeek(userAvailability.getDay().getDayOfWeek())
                                        .from(timeSlot.getFromTime())
                                        .to(timeSlot.getToTime())
                                        .build()
                        );
                    });
                });

                allSlots.add(userAvailableSlot);
            } catch (UserNotFoundException e) {
                log.warn(e.getMessage());
            }
        });

        Iterator<List<AvailableSlotsDTO>> it = allSlots.iterator();
        List<AvailableSlotsDTO> availableSlotsAcc = it.next();
        while (it.hasNext()) {
            List<AvailableSlotsDTO> next = it.next();
            availableSlotsAcc = distinct(availableSlotsAcc, next);
        }

        return availableSlotsAcc;
    }

    private List<AvailableSlotsDTO> distinct(List<AvailableSlotsDTO> list, List<AvailableSlotsDTO> otherList) {
        return new ArrayList<>(list.stream()
                .distinct()
                .filter(otherList::contains)
                .collect(Collectors.toSet()));
    }

    private TimeSlot buildTimeSlot(UserAvailabilityDTO.Time time) {
        return TimeSlot.builder()
                .fromTime(LocalTime.parse(time.getFromTime()))
                .toTime(LocalTime.parse(time.getToTime()))
                .build();
    }

}
