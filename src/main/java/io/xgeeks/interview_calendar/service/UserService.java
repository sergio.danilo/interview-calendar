package io.xgeeks.interview_calendar.service;

import io.xgeeks.interview_calendar.exception.UserNotFoundException;
import io.xgeeks.interview_calendar.model.entity.User;
import io.xgeeks.interview_calendar.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        throw new UserNotFoundException("User");
    }

    public void delete(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        optionalUser.ifPresentOrElse(userRepository::delete,
                () -> {
                    throw new UserNotFoundException("User");
                }
        );
    }

}
