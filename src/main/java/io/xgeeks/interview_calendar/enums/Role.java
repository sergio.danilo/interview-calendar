package io.xgeeks.interview_calendar.enums;

public enum Role {

    CANDIDATE, INTERVIEWER;

}
