package io.xgeeks.interview_calendar.repository;

import io.xgeeks.interview_calendar.model.entity.UserAvailability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAvailabilityRepository extends JpaRepository<UserAvailability, Long> {

    List<UserAvailability> findAllByUser_Id(Long userId);

}
