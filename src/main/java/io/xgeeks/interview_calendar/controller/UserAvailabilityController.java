package io.xgeeks.interview_calendar.controller;

import io.xgeeks.interview_calendar.model.dto.AvailableSlotsDTO;
import io.xgeeks.interview_calendar.model.dto.UserAvailabilityDTO;
import io.xgeeks.interview_calendar.model.entity.UserAvailability;
import io.xgeeks.interview_calendar.service.UserAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/availabilities")
public class UserAvailabilityController {

    @Autowired
    private UserAvailabilityService userAvailabilityService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void save(@Valid @RequestBody List<UserAvailabilityDTO> availabilities) {
        userAvailabilityService.save(availabilities);
    }

    @GetMapping("find_by_user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    List<UserAvailability> findByUser(@PathVariable Long userId) {
        return userAvailabilityService.findByUser(userId);
    }

    @GetMapping("available_slots")
    List<AvailableSlotsDTO> getAvailableSlots(@RequestBody List<Long> ids) {
        return userAvailabilityService.getAvailableSlots(ids);
    }

}
